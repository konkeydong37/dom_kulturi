let menu = document.querySelector('.menu'),
    menuButton = document.querySelector('.button-burger'),
    menuStub = document.querySelector('.menu__open-stub');


function open_menu(option) {
    option.addEventListener('click', function(evt) {
        evt.preventDefault();
    
        if (menu.classList.contains('open')) {
            menu.classList.remove('open');
        } else {
            menu.classList.add('open');
        }
    });
};

open_menu(menuButton);
open_menu(menuStub);

// Тач открытие меню

document.addEventListener('touchstart', handleTouchStart, false);        
document.addEventListener('touchmove', handleTouchMove, false);

let xDown = null;

function getTouches(evt) {
  return evt.touches
}                                                     

function handleTouchStart(evt) {
    const firstTouch = getTouches(evt)[0];                                      
    xDown = firstTouch.clientX;            
};                                                

function handleTouchMove(evt) {
    if ( ! xDown ) {
        return;
    }

    let xUp = evt.touches[0].clientX,
        xDiff = xDown - xUp;

    if ( xDown <= 25 ) {
        if ( xDiff < 0 ) {
            menu.classList.add('open')
        }                     
    } else {
        if ( xDiff > 0 ) {
            menu.classList.remove('open')
        } 
    }

    xDown = null;                                           
};

$(document).ready(function(){
    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        variableWidth: true,
        centerMode: true,
    });
});

$(window).scroll(function() {
    if ($(this).scrollTop() > 280){  
        $('button-back__warpper').addClass("black");
      }
      else{
        $('button-back__warpper').removeClass("black");
      }
});