var gulp = require("gulp"),
    less = require("gulp-less"),
    sass = require("gulp-sass"),
    sсss = require("gulp-scss"),
    plumber = require("gulp-plumber"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    server = require("browser-sync").create(),
    minify = require("gulp-clean-css"),
    rename = require("gulp-rename"),
    sourcemaps = require("gulp-sourcemaps"),
    imagemin = require("gulp-imagemin"),
    webp = require("gulp-webp"),
    svgstore = require("gulp-svgstore"),
    svgmin = require("gulp-svgmin"),
    path = require('path'),
    posthtml = require("gulp-posthtml"),
    include = require("posthtml-include"),
    del = require("del");

gulp.task("less", function () {
    return new Promise(function(resolve, reject) {
    gulp.src("../less/style.less")
      .pipe(plumber())
      .pipe(sourcemaps.init())
      .pipe(less())
      .pipe(postcss([
        autoprefixer({browsers: [
          "last 2 versions"
        ]})
      ]))
      .pipe(gulp.dest("../build/css"))
      .pipe(minify())
      .pipe(rename("style.min.css"))
      .pipe(sourcemaps.write('../css'))
      .pipe(gulp.dest("../build/css"))
      .pipe(server.stream());
      resolve();
    });
});

// gulp.task("sass", function () {
//     return new Promise(function(resolve, reject) {
//     gulp.src("../scss/bootstrap-grid.scss")
//       .pipe(plumber())
//       .pipe(sourcemaps.init())
//       .pipe(sass())
//       .pipe(postcss([
//         autoprefixer({browsers: [
//           "last 2 versions"
//         ]})
//       ]))
//       .pipe(gulp.dest("../build/css"))
//       .pipe(minify())
//       .pipe(rename("bootstrap-grid.min.css"))
//       .pipe(sourcemaps.write('../css'))
//       .pipe(gulp.dest("../build/css"))
//       .pipe(server.stream());
//       resolve();
//     });
// });

gulp.task("serve", function (done) {
  server.init({
    server: "../build/",
    injectChanges: true,
    open: false,
  });

  gulp.watch("../less/**/*.less", gulp.series("less"));
  // gulp.watch("../scss/**/*.scss", gulp.series("sass"));
  gulp.watch("../*.html", gulp.series("copy:html", "include"));
  gulp.watch("../images/sprite-icons/*.svg", gulp.series("svgstore"));
  gulp.watch("../sections/*.html", gulp.series("copy:html", "include"));
  gulp.watch("../*.html").on("change", server.reload);
  gulp.watch("../images/**/*.{png,jpg,svg}", gulp.series("images"));
  gulp.watch("../js/*.js", gulp.series("copy:js")).on("change", server.reload);

  done();
});

gulp.task("images", function () {
  return new Promise(function(resolve, reject) {
    gulp.src("../images/**/*.{png,jpg,svg}")
    .pipe(imagemin([
      imagemin.optipng({optimizationLevel: 3}),
      imagemin.jpegtran({progressive: true}),
      imagemin.svgo({
        plugins: [
            {convertShapeToPath: false},
            {cleanupIDs: true},
            {removeStyleElement: false}
        ]
      }),
    ]))
    .pipe(gulp.dest("../build/images"));
    resolve();
  })
});

// gulp.task("webp", function () {
//   return gulp.src("images/**/*.{png,jpg}")
//     .pipe(webp({quality: 90}))
//     .pipe(gulp.dest("build/images"));
// });

gulp.task("svgstore", function () {
  return new Promise(function(resolve, reject) {
    gulp.src("../images/sprite-icons/*.svg")
      .pipe(svgmin(function (file) {
        var prefix = path.basename(file.relative, path.extname(file.relative));
        return {
            plugins: [{
                cleanupIDs: {
                    prefix: prefix + '-',
                    minify: true
                }
              }]
            }
        }))
      .pipe(svgstore())
      .pipe(gulp.dest("../build/images/sprite-icons/sprite"));
      resolve();
  });
});

// gulp.task('svgstore', function () {
//   var svgs = gulp
//       .src("../build/images/icons/*.svg")
//       .pipe(svgstore({ inlineSvg: true }));

//   function fileContents (filePath, file) {
//       return file.contents.toString();
//   }

//   return gulp
//       .src('build/images/icons/inline-svg.html')
//       .pipe(inject(svgs, { transform: fileContents }))
//       .pipe(gulp.dest('build/images/icons'));
// });

// gulp.task("sprite-for-work", function () {
//   return gulp.src("images/icons/icon-*.svg")
//     .pipe(svgstore({inlineSvg: true}))
//     .pipe(rename("sprite.svg"))
//     .pipe(gulp.dest("images/icons"));
// });

gulp.task("include", function () {
  return new Promise(function(resolve, reject) {
      gulp.src("../*.html")
      .pipe(posthtml([include()]))
      .pipe(gulp.dest("../build"));
      resolve();
  });
});

gulp.task("copy", function () {
  return gulp.src([
      "../fonts/**/*.{woff,woff2}",
      "../js/**",
      // "../*.html",
      // "../css/*.css"
    ], {
      base: "../"
    })
    .pipe(gulp.dest("../build"));
});

gulp.task("copy:html", function () {
  return gulp.src([
      "../*.html"
    ], {
      base: "../"
    })
    .pipe(gulp.dest("../build"));
});

gulp.task("copy:images", function () {
  return gulp.src([
    "../images/**/*.{png,jpg,svg}"
  ], {
    base: "../"
  })
  .pipe(gulp.dest("../build"));
});

gulp.task("copy:js", function () {
  return gulp.src([
    "../js/*.js"
  ], {
    base: "../"
  })
  .pipe(gulp.dest("../build"))
});

gulp.task("clean", function () {
  return del("../build", {force: true});
});

gulp.task("build", gulp.series("clean", "copy", "less", "images", "include"));
